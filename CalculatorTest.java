import org.junit.*;
import static org.junit.Assert.*;

public class CalculatorTest{

    @Test
    public void testAdd(){
	double n1 = 2.5;
	double n2 = 0;
	double expected = 2.5;
	double result = Calculator.add(n1, n2);
	assertEquals(expected, result, 1e-6);
    }

    @Test
    public void testSubtract(){
	double expectedResult = 1;
	assertEquals(expectedResult, Calculator.subtract(2, 1), 1e-6);
    }
    @Test
    public void testMultiply(){
	double n1 = 2;
	double n2 = 2;
	double expected = 4;
	double result = Calculator.multiply(n1, n2);
	assertEquals(expected, result, 1e-6);
    }
    @Test
    public void testDivide(){
	double n1 = 10;
	double n2 = 2;
	double expected = 5;
	double result = Calculator.divide(n1, n2);
	assertEquals(expected, result, 1e-6);
    }
    @Test
    public void testAbs(){
	double n = -0.1;
	double expected = 0.1;
	double result = Calculator.abs(n);
	assertEquals(expected, result, 1e-6);
    }
        @Test
    public void testPower(){
	    double n = 4;
	    int  m = -2;
	double expected = 0.0625;
	double result = Calculator.power(n, m);
	assertEquals(expected, result, 1e-6);
    }

    
}
